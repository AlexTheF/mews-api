<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('customers')->group(function () {
    Route::post('/list', 'App\Http\Controllers\Api\v1\CustomersController@list');
    Route::post('/find', 'App\Http\Controllers\Api\v1\CustomersController@find');
    Route::post('/open_items', 'App\Http\Controllers\Api\v1\CustomersController@openItems');
    Route::post('/add', 'App\Http\Controllers\Api\v1\CustomersController@add');
    Route::post('/edit', 'App\Http\Controllers\Api\v1\CustomersController@edit');
    Route::post('/reservations', 'App\Http\Controllers\Api\v1\CustomersController@reservations');
});

Route::prefix('reservations')->group(function () {
    Route::post('/list', 'App\Http\Controllers\Api\v1\ReservationsController@list');
    Route::post('/add', 'App\Http\Controllers\Api\v1\ReservationsController@add');
    Route::post('/confirm', 'App\Http\Controllers\Api\v1\ReservationsController@confirm');
    Route::post('/cancel', 'App\Http\Controllers\Api\v1\ReservationsController@cancel');
});

Route::prefix('services')->group(function () {
    Route::post('/list', 'App\Http\Controllers\Api\v1\ServicesController@list');
    Route::post('/rates_list', 'App\Http\Controllers\Api\v1\ServicesController@rates');
});
