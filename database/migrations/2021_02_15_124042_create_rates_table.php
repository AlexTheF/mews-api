<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->id();
            $table->string('base_rate_id', 36)->nullable();
            $table->string('external_id', 36)->nullable();
            $table->string('group_id', 36)->nullable();
            $table->string('service_id', 36)->nullable();
            $table->boolean('is_active');
            $table->boolean('is_enabled');
            $table->boolean('is_public');
            $table->string('name', 150);
            $table->string('short_name', 150);
            $table->timestamps();
            $table->unique('external_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rates');
    }
}
