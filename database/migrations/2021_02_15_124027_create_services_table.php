<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('external_id', 36);
            $table->string('name', 150);
            $table->string('start_time', 50)->nullable();
            $table->string('end_time', 50)->nullable();
            $table->boolean('is_active');
            $table->string('type', 50);
            $table->timestamps();
            $table->unique('external_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicess');
    }
}
