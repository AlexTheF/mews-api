<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('external_id', 36);
            $table->date('birthday')->nullable();
            $table->string('birth_place', 150)->nullable();
            $table->string('category_id', 36)->nullable();
            $table->string('email', 150)->nullable();
            $table->string('first_name', 150)->nullable();
            $table->string('last_name', 150)->nullable();
            $table->string('second_last_name', 150)->nullable();
            $table->string('sex', 10)->nullable();
            $table->string('locale', 10)->nullable();
            $table->string('phone', 16)->nullable();
            $table->timestamps();
            $table->unique('external_id');
            $table->index('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
