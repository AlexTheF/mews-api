<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();
            $table->string('external_id', 36);
            $table->string('service_id', 36);
            $table->string('group_id', 36);
            $table->string('customer_id', 36);
            $table->string('booker_id', 36)->nullable();
            $table->string('rate_id', 36);
            $table->string('number', 20);
            $table->string('state', 20);
            $table->string('origin', 20);
            $table->string('segment', 36);
            $table->integer('adults_count');
            $table->integer('children_count');
            $table->string('cancellation_reason', 255)->nullable();
            $table->dateTimeTz('start_utc');
            $table->dateTimeTz('end_utc');
            $table->dateTimeTz('cancelled_at');
            $table->dateTimeTz('deleted_at')->nullable();
            $table->timestamps();
            $table->unique('external_id');
            $table->index('service_id');
            $table->index('group_id');
            $table->index('customer_id');
            $table->index('booker_id');
            $table->index('rate_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
