<?php

return [
    'client' => env('MEWS_CLIENT', ''),
    'client_token' => env('MEWS_CLIENT_TOKEN', ''),
    'access_token' => env('MEWS_ACCESS_TOKEN', ''),
    'mode' => env('MEWS_MODE', ''),
];
