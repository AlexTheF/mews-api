<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [
        'external_id',
        'name',
        'start_time',
        'end_time',
        'is_active',
        'type'
    ];

    public function reservation () {
        $this->hasMany(Reservation::class, 'service_id', 'external_id');
    }
}
