<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = ['external_id',
        'service_id',
        'customer_id',
        'group_id',
        'booker_id',
        'rate_id',
        'state',
        'number',
        'origin',
        'segment',
        'adults_count',
        'children_count',
        'cancellation_reason',
        'start_utc',
        'end_utc',
        'cancelled_at',
        'created_at',
        'updated_at'
    ];

    use HasFactory;

    public function customer () {
        return $this->belongsTo(Customer::class, 'booker_id', 'external_id');
    }

    public function booker () {
        return $this->belongsTo(Customer::class, 'customer_id', 'external_id');
    }

    public function service () {
        return $this->belongsTo(Service::class, 'service_id', 'external_id');
    }

    public function Rate () {
        return $this->belongsTo(Rate::class, 'rate_id', 'external_id');
    }
}
