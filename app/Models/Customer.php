<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
         'external_id',
         'birthday',
         'birth_place',
         'category_id',
         'email',
         'first_name',
         'last_name',
         'second_last_name',
         'sex',
         'locale',
         'phone'
    ];
    use HasFactory;

    public function reservations () {
        return $this->hasMany(Reservation::class, 'customer_id', 'external_id');
    }
}
