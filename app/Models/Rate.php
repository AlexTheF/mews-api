<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use HasFactory;

    protected $fillable = [
        'base_rate_id',
        'external_id',
        'group_id',
        'service_id',
        'name',
        'short_name',
        'is_active',
        'is_enabled',
        'is_public'
    ];

    public function reservations () {
        $this->hasMany(Reservation::class, 'rate_id', 'external_id');
    }
}
