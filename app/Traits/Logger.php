<?php

namespace App\Traits;

trait Logger {

    public static function logAndReturn($message) :array {
        return ['status' => 'error', 'message' => $message];
    }
}
