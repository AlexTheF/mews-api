<?php

namespace App\Connector;

use Illuminate\Support\Facades\Http;
use App\Traits\Logger;

class MewsConnector {

    use Logger;

    private $request;

    private $resource = '';

    private $requestUrl = '';

    private $endpoints = [
        'demo' => 'https://demo.mews.li/api/connector/v1/',
        'live' => 'https://www.mews.li/api/connector/v1/',
    ];

    public function __construct(String $clientToken, String $accessToken, String $client, String $state = 'demo') {
        $this->request = [
            "ClientToken" => $clientToken,
            "AccessToken" => $accessToken,
            "Client" => $client,
        ];
        $this->requestUrl = $this->endpoints[$state];
    }

    public function setResource (String $resource) :MewsConnector {
        $this->resource = $resource;
        return $this;
    }

    public function process (String $method, $requestData = []) :array {
        $requestData = array_merge($this->request, $requestData);

        $response = Http::post($this->requestUrl . $this->resource . '/' . $method, $requestData);
        if ($response->ok()) {
            return $response->json();
        }
        return Logger::logAndReturn($response->body());
    }
}
