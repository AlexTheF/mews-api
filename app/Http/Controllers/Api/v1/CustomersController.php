<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\AddCustomer;
use App\Http\Requests\Customer\CustomerList;
use App\Models\Customer;
use App\ServiceProviders\CustomersProvider;
use App\ServiceProviders\ReservationsProvider;
use Illuminate\Http\Request;

class CustomersController extends Controller {

    public function list (CustomerList $request, CustomersProvider $customers) {
        $customers_list = $customers->getCustomersList($request->all());
        foreach ($customers_list['Customers'] AS $customer) {
            $data = [];
            foreach (CustomersProvider::$mapping_rules as $key => $field) {
                $data[$field] = $customer[$key];
            }
            Customer::updateOrCreate($data);
        }

        return $customers_list;
    }

    public function find (Request $request, CustomersProvider $customers) {
        $request->validate([
            'name' => 'required_without:ResourceId|string',
            'ResourceId' => 'required_without:name|string'
        ]);
        return $customers->searchCustomers($request->all());
    }

    public function openItems (Request $request, CustomersProvider $customers) {
        $request->validate([
            'CustomerIds' => 'array',
            'CustomerIds.*' => 'required|string'
        ]);
        return $customers->getCustomerOpenItems($request->all());
    }

    public function add (AddCustomer $request, CustomersProvider $customers) {
        return $customers->addCustomer($request->all());
    }

    public function edit (AddCustomer $request, CustomersProvider $customers) {
        return $customers->updateCustomer($request->all());
    }

    public function reservations (Request $request, ReservationsProvider $reservations) {
        $request->validate([
            'CustomerIds' => 'required|array',
            'CustomerIds.*' => 'required|string'
        ]);
        return $reservations->getReservationsList($request->all());
    }
}
