<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Reservation\AddReservation;
use App\Http\Requests\Reservation\ReservationsList;
use App\Models\Customer;
use App\Models\Reservation;
use App\ServiceProviders\CustomersProvider;
use App\ServiceProviders\ReservationsProvider;
use Illuminate\Http\Request;

class ReservationsController extends Controller {

    public function list (ReservationsList $criteria, ReservationsProvider $reservations) {
        $reservations_list = $reservations->getReservationsList($criteria->all());
        if (empty($reservations_list['Reservations'])) {
            return [];
        }
        foreach ($reservations_list['Reservations'] AS $reservation) {
            $data = [];
            foreach (ReservationsProvider::$mapping_rules as $key => $field) {
                if (is_array($field) && $field[1] === 'date') {
                    $data[$field[0]] = date('Y-m-d H:i:s', strtotime($reservation['$key']));
                } else {
                    $data[$field] = $reservation[$key];
                }
            }
            Reservation::updateOrCreate($data);
        }
        if (!empty($reservations_list['Reservations'])) {
            foreach ($reservations_list['Customers'] AS $customer) {
                $data = [];
                foreach (CustomersProvider::$mapping_rules as $key => $field) {
                    $data[$field] = $customer[$key];
                }
                Customer::updateOrCreate($data);
            }
        }
        return $reservations_list;
    }

    public function add (AddReservation $data, ReservationsProvider $reservations) {
        return $reservations->addReservation($data->all());
    }

    public function confirm (Request $request, ReservationsProvider $reservations) {
        $request->validate([
            'ReservationIds' => 'array',
            'ReservationIds.*' => 'required|string'
        ]);
        return $reservations->confirmReservation($request->all());
    }

    public function cancel (Request $request, ReservationsProvider $reservations) {
        $request->validate([
            'ReservationIds' => 'array',
            'ReservationIds.*' => 'required|string',
            "ChargeCancellationFee" => 'required:boolean',
            "Notes" => "string"
        ]);
        return $reservations->cancelReservation($request->all());
    }
}
