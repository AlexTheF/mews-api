<?php
namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Rate;
use App\ServiceProviders\RatesProvider;
use App\ServiceProviders\ServicesProvider;
use Illuminate\Http\Request;

class ServicesController extends Controller {
    public function list (ServicesProvider $services) {
        $services_list = $services->getServicesList([]);
        if (empty($services_list['Services'])) {
            return [];
        }
        foreach ($services_list['Services'] as $service) {
            $data = [];
            foreach (ServicesProvider::$mapping_rules as $key => $field) {
                $data[$field] = $service[$key];
            }
            Service::updateOrCreate($data);
        }
        return $services_list;
    }

    public function rates (Request $request, RatesProvider $rates) {
        $request->validate([
            'ServiceIds' => 'required|array',
            'ServiceIds.*' => 'required|string'
        ]);
        $rates_list = $rates->getRatesList([]);
        if (empty($rates_list['Rates'])) {
            return [];
        }
        foreach ($rates_list['Rates'] as $rate) {
            $data = [];
            foreach (RatesProvider::$mapping_rules as $key => $field) {
                $data[$field] = $rate[$key];
            }
            Rate::updateOrCreate($data);
        }
        return $rates_list;
    }
}
