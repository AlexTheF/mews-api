<?php

namespace App\Http\Requests\Reservation;

use Illuminate\Foundation\Http\FormRequest;

class ReservationsList extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "StartUtc" => "required|date_format:Y-m-d\TH:i:sO",
            "EndUtc" => "required|date_format:Y-m-d\TH:i:sO",
            "ServiceIds" => 'array',
            "ServiceIds.*" => 'required|string',
            "ReservationIds" => 'array',
            "ReservationIds.*" => 'required|string',
            "CustomerIds" => 'array',
            "CustomerIds.*" => 'required|string',
            "RateIds" => 'array',
            "RateIds.*" => 'required|string',
            "States" => 'array',
            "States.*" => 'required|string',
            "Extent" => 'array',
            "Extent.Reservations" => 'boolean',
            "Extent.ReservationGroups" => 'boolean',
            "Extent.Customers" => 'boolean',
        ];
    }
}
