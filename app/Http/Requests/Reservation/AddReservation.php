<?php

namespace App\Http\Requests\Reservation;

use Illuminate\Foundation\Http\FormRequest;

class AddReservation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "ServiceId" => "required:string",
            "GroupId" => 'string',
            "GroupName" => 'string',
            "SendConfirmationEmail" => 'boolean',
            "CheckRateApplicability" => 'boolean',
            "CheckOverbooking" => 'boolean',
            "Reservations" => 'required|array',
            "Reservations.*" => 'array',
            "Reservations.*.Identifier" => "string",
            "Reservations.*.State" => "string",
            "Reservations.*.StartUtc" => "required|date_format:Y-m-d\TH:i:sO",
            "Reservations.*.EndUtc" => "required|date_format:Y-m-d\TH:i:sO",
            "Reservations.*.ReleasedUtc" => 'date_format:Y-m-d\TH:i:sO',
            "Reservations.*.AdultCount" => 'required|numeric',
            "Reservations.*.ChildCount" => 'required|numeric',
            "Reservations.*.CustomerId" => "required:string",
            "Reservations.*.BookerId" => "string",
            "Reservations.*.RequestedCategoryId" => "required:string",
            "Reservations.*.RateId" => "required:string",
            "Reservations.*.TravelAgencyId" => 'string',
            "Reservations.*.CompanyId" => 'string',
            "Reservations.*.Notes" => "string",
            "Reservations.*.TimeUnitAmount" => 'array',
            "TimeUnitPrices" => 'array',
            "ProductOrders" => 'array',
            "ProductOrders.ProductId" => 'required:string',
            "ProductOrders.StartUtc" => 'required|date_format:Y-m-d\TH:i:sO',
            "ProductOrders.EndUtc" => 'required|date_format:Y-m-d\TH:i:sO',
            "AvailabilityBlockId" => 'string'
        ];
    }
}
