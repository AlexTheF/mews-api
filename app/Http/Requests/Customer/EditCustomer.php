<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class EditCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "CustomerId" => 'required|string',
            "FirstName" => "string",
            "LastName" => "required|string",
            "SecondLastName" => "string",
            "Title" => "string",
            "Sex" => "string",
            "NationalityCode" => "string",
            "BirthDate" => "date_format:Y-m-d",
            "BirthPlace" => "string",
            "Email" => "email",
            "Phone" => "numeric",
            "LoyaltyCode" => 'string',
            "Notes" => 'string',
            "IdentityCard" => 'array',
            "IdentityCard.Number" => 'numeric',
            "IdentityCard.Expiration" => 'date_format:Y-m-d',
            "IdentityCard.Issuance" => 'date_format:Y-m-d',
            "IdentityCard.IssuingCountryCode" => 'string',
            "Passport" => 'array',
            "Passport.id" => 'required|string',
            "Passport.CustomerId" => 'required|string',
            "Passport.Type" => 'required:string',
            "Passport.Number" => 'string',
            "Passport.Expiration" => 'date_format:Y-m-d\TH:i:sO',
            "Passport.Issuance" => 'date_format:Y-m-d\TH:i:sO',
            "Passport.IssuingCountryCode" => 'string',
            "Visa" => 'array',
            "DriversLicense" => 'array',
            "Address" => 'array',
            "Address.Line1" => 'string',
            "Address.Line2" => 'string',
            "Address.City" => 'string',
            "Address.PostalCode" => 'string',
            "Address.CountryCode" => 'string',
            "Address.CountrySubdivisionCode" => 'string',
            "ItalianDestinationCode" => "string",
            "Classifications" => "array"
        ];
    }
}
