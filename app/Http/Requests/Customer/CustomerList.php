<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class CustomerList extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "CustomerIds" => "array",
            "CustomerIds.*" => "required|string",
            "Emails" => "array",
            "Emails.*" => "required|string",
            "FirstNames" => "array",
            "FirstNames.*" => "required|string",
            "LastNames" => "array",
            "LastNames.*" => "required|string",
            "LoyaltyCodes" => "array",
            "LoyaltyCodes.*" => "required|string",
            "CreatedUtc" => "array",
            "CreatedUtc.StartUtc" => "date_format:Y-m-d\TH:i:sO",
            "CreatedUtc.EndUtc" => "date_format:Y-m-d\TH:i:sO",
            "UpdatedUtc.StartUtc" => "date_format:Y-m-d\TH:i:sO",
            "UpdatedUtc.EndUtc" => "date_format:Y-m-d\TH:i:sO",
            "Extent"  => "array"
        ];
    }
}
