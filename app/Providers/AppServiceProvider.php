<?php

namespace App\Providers;

use App\ServiceProviders\CustomersProvider;
use App\ServiceProviders\RatesProvider;
use App\ServiceProviders\ReservationsProvider;
use App\ServiceProviders\ServicesProvider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CustomersProvider::class, function () {
            return new CustomersProvider();
        });
        $this->app->singleton(ReservationsProvider::class, function () {
            return new ReservationsProvider();
        });
        $this->app->singleton(ServicesProvider::class, function () {
            return new ServicesProvider();
        });
        $this->app->singleton(RatesProvider::class, function () {
            return new RatesProvider();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
