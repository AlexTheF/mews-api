<?php

namespace App\ServiceProviders;

class CustomersProvider extends baseProvider {

    protected $connector;

    private $resource = 'customers';

    public static $mapping_rules = [
        'Id' => 'external_id',
        'BirthDate' => 'birthday',
        'BirthPlace' => 'birth_place',
        'CategoryId' => 'category_id',
        'Email' => 'email',
        'FirstName' => 'first_name',
        'LastName' => 'last_name',
        'SecondLastName' => 'second_last_name',
        'Sex' => 'sex',
        'LanguageCode' => 'locale',
        'Phone' => 'phone'
    ];

    public function __construct () {
        parent::__construct();
        $this->connector = $this->connector->setResource($this->resource);
    }

    public function getCustomersList (array $criteria) :array {
        return $this->connector->process('getAll', $criteria);
    }

    public function searchCustomers (array $criteria) :array {
        return $this->connector->process('search', $criteria);
    }

    public function getCustomerOpenItems (array $criteria) :array {
        return $this->connector->process('getOpenItems', $criteria);
    }

    public function addCustomer (array $data) :array {
        return $this->connector->process('add', $data);
    }

    public function updateCustomer (array $data) :array {
        return $this->connector->process('update', $data);
    }

    public function mergeCustomers ($source = '', $target = '') :array {
        return $this->connector->process('merge', [
            "SourceCustomerId" => $source,
            "TargetCustomerId" => $target
        ]);
    }
}
