<?php

namespace App\ServiceProviders;

class RatesProvider extends baseProvider {

    protected $connector;

    private $resource = 'rates';

    public static $mapping_rules = [
        'BaseRateId' => 'base_rate_id',
        'Id' => 'external_id',
        'GroupId' => 'group_id',
        'ServiceId' => 'service_id',
        'Name' => 'name',
        'ShortName' => 'short_name',
        'IsActive' => 'is_active',
        'IsEnabled' => 'is_enabled',
        'IsPublic' => 'is_public'
    ];

    public function __construct () {
        parent::__construct();
        $this->connector = $this->connector->setResource($this->resource);
    }

    public function getRatesList (array $criteria) :array {
        return $this->connector->process('getAll', $criteria);
    }
}
