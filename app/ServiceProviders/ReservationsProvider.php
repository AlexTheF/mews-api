<?php

namespace App\ServiceProviders;

class ReservationsProvider extends baseProvider {

    protected $connector;

    private $resource = 'reservations';

    public static $mapping_rules = [
        'Id' => 'external_id',
        'ServiceId' => 'service_id',
        'CustomerId' => 'customer_id',
        'GroupId' => 'group_id',
        'BookerId' => 'booker_id',
        'RateId' => 'rate_id',
        'State' => 'state',
        'Number' => 'number',
        'Origin' => 'origin',
        'BusinessSegmentId' => 'segment',
        'AdultCount' => 'adults_count',
        'ChildCount' => 'children_count',
        'CancellationReason' => 'cancellation_reason',
        'StartUtc' => ['start_utc', 'date'],
        'EndUtc' => ['end_utc', 'date'],
        'CancelledUtc' => ['cancelled_at', 'date'],
        'CreatedUtc' => ['created_at', 'date'],
        'UpdatedUtc' => ['updated_at', 'date'],
    ];

    public function __construct () {
        parent::__construct();
        $this->connector = $this->connector->setResource($this->resource);
    }

    public function getReservationsList (array $criteria) :array {
        return $this->connector->process('getAll', $criteria);
    }

    public function addReservation (array $data) :array {
        return $this->connector->process('add', $data);
    }

    public function confirmReservation (array $data) :array {
        return $this->connector->process('confirm', $data);
    }

    public function cancelReservation (array $data) :array {
        return $this->connector->process('cancel', $data);
    }
}
