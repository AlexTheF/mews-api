<?php

namespace App\ServiceProviders;

class ServicesProvider extends baseProvider {

    protected $connector;

    private $resource = 'services';

    public static $mapping_rules = [
        'Id' => 'external_id',
        'Name' => 'name',
        'StartTime' => 'start_time',
        'EndTime' => 'end_time',
        'IsActive' => 'is_active',
        'Type' => 'type'
    ];

    public function __construct () {
        parent::__construct();
        $this->connector = $this->connector->setResource($this->resource);
    }

    public function getServicesList (array $criteria) :array {
        return $this->connector->process('getAll', $criteria);
    }
}
