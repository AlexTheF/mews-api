<?php

namespace App\ServiceProviders;

use App\Connector\MewsConnector;

class baseProvider {

    protected $connector;

    public function __construct () {
        $this->connector = new MewsConnector(config('mews.client_token'), config('mews.access_token'), config('mews.client'), config('mews.mode'));
    }
}
